import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {DatePipe} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {EditPurchaseComponent} from './edit-purchase/edit-purchase.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EditPurchaseComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'ng-cli-universal'}),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent, pathMatch: 'full'},
    ]),
    ReactiveFormsModule,
    NgbModule

  ],
  providers: [ DatePipe],
  bootstrap: [AppComponent],
  entryComponents: [EditPurchaseComponent]
})
export class AppModule { }
