import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {PurchaseCategoryName} from '../models/purchaseCategoryName';
import {Category} from '../models/Category';

@Component({
  selector: 'app-edit-purchase',
  templateUrl: './edit-purchase.component.html',
  styleUrls: ['./edit-purchase.component.css']
})
export class EditPurchaseComponent implements OnInit {

  @Input() public purchase: PurchaseCategoryName;
  @Input() public categories: Category[];
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  passBack() {
    this.activeModal.close(this.purchase);
  }

}
