import { Component, OnInit } from '@angular/core';
import {PurchaseService} from '../purchase.service';
import * as _ from 'lodash';
import {PurchaseCategoryName} from '../models/purchaseCategoryName';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Category} from '../models/Category';
import {Purchase} from '../models/purchase';
import {DatePipe} from '@angular/common';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EditPurchaseComponent} from '../edit-purchase/edit-purchase.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  public editPurchaseCategoryName: PurchaseCategoryName;
  public purchases: PurchaseCategoryName[] = [];
  public categories: Category[] = [];
  public purchaseCount  = 0;
  public purchaseAmount  = 0;
  public myForm: FormGroup;

  public tester: Date;

  constructor(public purchaseService: PurchaseService, public datePipe: DatePipe, private modalService: NgbModal) {}

  public get averageAmountOfPurchase() {
    return _.round(this.purchaseAmount / this.purchaseCount, 2);
  }

  ngOnInit() {
    this.myForm = new FormGroup({
      name: new FormControl('', [<any>Validators.required]),
      date: new FormControl('', [<any>Validators.required]),
      amount: new FormControl('', [<any>Validators.required]),
      categoryId: new FormControl(null, [<any>Validators.required]),
      memo: new FormControl('', []),
    });
    this.tester = new Date();
    this.purchaseService.getPurchases().subscribe((res) => this.onSuccess(res));
    this.purchaseService.getCategories().subscribe((res: Category[]) => this.displayCategories(res));
    this.clearForm();
  }

  add() {
    const newItem: PurchaseCategoryName = new PurchaseCategoryName({categoryName: 'Personal',
                                                                          categoryId: 5,
                                                                          name: 'Added',
                                                                          amount: 22.50,
                                                                          date: '2020-04-11',
                                                                          memo: ''});
    this.purchaseService.addPurchase(newItem).subscribe((res) => {
      const item = new PurchaseCategoryName(res);
      item.categoryName = newItem.categoryName;
      this.purchases.push(item);
      this.sortPurchases(this.purchases);
      this.reTotalPurchases();
    });
  }

  onSuccess(res) {
    const tempPurchases = [];
    if (res !== undefined) {
      res.forEach(item => {
        tempPurchases.push(new PurchaseCategoryName(item));
      });
    }
    this.sortPurchases((tempPurchases));
    this.reTotalPurchases();
  }

  displayCategories(res) {
    this.categories = res;
  }

  sortPurchases(purchases) {
    // sorted ascending
    this.purchases = _.orderBy(purchases, [p => new Date(p.date).getTime()], ['asc']);
  }

  reTotalPurchases() {
    this.purchaseCount = this.purchases.length;
    this.purchaseAmount = _.round(_.sumBy(this.purchases, 'amount'), 2);
  }

  public save(value: any, valid: boolean) {
    if (valid) {
      const tempPurchase = new Purchase(value);
      tempPurchase.categoryId = Number(value.categoryId);
      tempPurchase.amount = Number(value.amount);
      this.purchaseService.addPurchase(tempPurchase).subscribe((res) => {
        console.log(res);
        const item = new PurchaseCategoryName(res);
        item.categoryName = this.lookupCategoryName(item.categoryId);
        this.purchases.push(item);
        this.sortPurchases(this.purchases);
        this.reTotalPurchases();
        this.clearForm();
      });
    }
  }

  isValid(control) {
    return this.myForm.controls[control].invalid && this.myForm.controls[control].touched;
  }

  lookupCategoryName(id: number): string {
    const item = _.find(this.categories, ['id', id]);
    return item.categoryName;
  }

  clearForm() {

    this.myForm.controls['name'].reset('');
    this.myForm.controls['date'].reset(this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
    this.myForm.controls['amount'].reset('');
    this.myForm.controls['categoryId'].reset(null);
    this.myForm.controls['memo'].reset('');
  }

  fillForm() {
    this.myForm.controls['name'].setValue('Value');
    this.myForm.controls['date'].setValue('1987-06-15');
    this.myForm.controls['amount'].setValue('200.01');
    this.myForm.controls['categoryId'].setValue(5);
    this.myForm.controls['memo'].setValue('This is a very boriing memo');
  }

  openModal(purchase: PurchaseCategoryName) {
    const modalRef = this.modalService.open(EditPurchaseComponent);
    const tempPurchase = new PurchaseCategoryName(purchase);
    modalRef.componentInstance.purchase = tempPurchase;
    modalRef.componentInstance.categories = this.categories;
    modalRef.result.then((result) => {
      result.categoryId = Number(result.categoryId);
      this.purchaseService.putPurchase(result).subscribe((putResult) => {
        this.purchases = this.purchases.filter(obj => obj !== purchase);
        result.categoryName = this.lookupCategoryName(result.categoryId);
        this.purchases.push(result);
        this.sortPurchases(this.purchases);
        this.reTotalPurchases();
      });
    });
  }

}
