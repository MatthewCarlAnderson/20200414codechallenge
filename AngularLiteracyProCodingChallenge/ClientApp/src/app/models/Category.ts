export class Category {
  constructor(data?) {
    Object.assign(this, data);
  }
  public id: number;
  public categoryName: string;

}
