export class Purchase {
  constructor(data?) {
    Object.assign(this, data);
  }

  public id: number;
  public name: string;
  public categoryId: number;
  public date: Date;
  public amount: number;
  public memo: string;
}
