import {Purchase} from './purchase';

export class PurchaseCategoryName extends  Purchase {
  constructor(data?) {
    super();
    Object.assign(this, data);
  }
  public categoryName: string;

}
