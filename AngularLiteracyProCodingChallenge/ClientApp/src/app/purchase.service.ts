import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PurchaseCategoryName} from './models/purchaseCategoryName';
import {Purchase} from './models/purchase';
import {Category} from './models/Category';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {
  private BASE_URL = 'https://localhost:44367';
s
  constructor(private http: HttpClient) { }

  getPurchases() {
    return this.http.get<PurchaseCategoryName[]>(this.BASE_URL + '/purchasecategoryname')
  }

  addPurchase(p: Purchase) {
    return this.http.post(this.BASE_URL + '/purchase', p);
  }

  putPurchase(p: Purchase) {
    return this.http.put(this.BASE_URL + '/purchase/' + p.id, p);
  }

  getCategories() {
    return this.http.get<Category[]>(this.BASE_URL + '/category');
  }
}
