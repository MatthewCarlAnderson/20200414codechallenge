﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularLiteracyProCodingChallenge.Data;
using AngularLiteracyProCodingChallenge.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;


namespace AngularLiteracyProCodingChallenge.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PurchaseCategoryNameController
    {
        private readonly DataContext _context;
        public PurchaseCategoryNameController(DataContext context)
        {
            _context = context;
        }

        // GET: api/PurchaseCategoryName
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PurchaseCategoryName>>> GetItems()
        {
            var items = (from p in _context.Purchase
                         join c in _context.Category on p.CategoryId equals c.Id
                         orderby p.Date
                         select new PurchaseCategoryName() { 
                             Id = p.Id, 
                             CategoryId = p.CategoryId,
                             Amount = p.Amount,
                             Date = p.Date,
                             Name = p.Name,
                             Memo = p.Memo,
                             CategoryName = c.CategoryName
                         });
            return await items.ToListAsync();
        }
    }
}
