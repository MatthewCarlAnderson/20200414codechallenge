﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularLiteracyProCodingChallenge.Data
{
    public class DataInitializer
    {
        public void SeedData(DataContext context)
        {
            context.Database.EnsureCreated();
            if (context.Category.Any())
            {
                return; //database has been seeded
            }
            CreateCategories(context);
        }

        public void CreateCategories(DataContext context)
        {
            context.Category.Add(new Models.Category { Id = 1, CategoryName = "Personal" });
            context.Category.Add(new Models.Category { Id = 2, CategoryName = "Business" });
        }
    }
}
