﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AngularLiteracyProCodingChallenge.Models
{
    public class Purchase
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }

        public DateTime Date { get; set; }

        public string Memo { get; set; }
    }
}
