﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularLiteracyProCodingChallenge.Models
{
    public class PurchaseCategoryName: Purchase
    {
        public string CategoryName { get; set; }
    }
}
