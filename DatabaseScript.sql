USE [master]
GO
/****** Object:  Database [LiteracyProCodingChallengeMAnderson]    Script Date: 4/14/2020 7:07:30 AM ******/
CREATE DATABASE [LiteracyProCodingChallengeMAnderson]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LiteracyProCodingChallengeMAnderson', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQL2017\MSSQL\DATA\LiteracyProCodingChallengeMAnderson.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LiteracyProCodingChallengeMAnderson_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQL2017\MSSQL\DATA\LiteracyProCodingChallengeMAnderson_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LiteracyProCodingChallengeMAnderson].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET ARITHABORT OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET RECOVERY FULL 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET  MULTI_USER 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'LiteracyProCodingChallengeMAnderson', N'ON'
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET QUERY_STORE = OFF
GO
USE [LiteracyProCodingChallengeMAnderson]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [LiteracyProCodingChallengeMAnderson]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 4/14/2020 7:07:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Purchase]    Script Date: 4/14/2020 7:07:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Purchase](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Date] [date] NOT NULL,
	[Memo] [ntext] NULL,
 CONSTRAINT [PK_Purchase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Purchase]  WITH CHECK ADD FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([Id])
GO
USE [master]
GO
ALTER DATABASE [LiteracyProCodingChallengeMAnderson] SET  READ_WRITE 
GO

USE [LiteracyProCodingChallengeMAnderson]

SET IDENTITY_INSERT Category ON
INSERT INTO [dbo].[Category] (Id, CategoryName) VALUES (1,'Personal')
INSERT INTO [dbo].[Category] (Id, CategoryName) VALUES (2,'Business')
SET IDENTITY_INSERT Category OFF

INSERT INTO [dbo].[Purchase] (CategoryId, [Name], Amount, [Date], [Memo]) VALUES
								(1, 'Gym Membership', 200.75, '2020-01-01', null)
INSERT INTO [dbo].[Purchase] (CategoryId, [Name], Amount, [Date], [Memo]) VALUES
								(1, 'King Soopers', 111.43, '2020-01-03', null)
INSERT INTO [dbo].[Purchase] (CategoryId, [Name], Amount, [Date], [Memo]) VALUES
								(2, 'Microsoft', 495.00, '2020-01-04', 'MSDN License')
GO