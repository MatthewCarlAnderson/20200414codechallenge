# README #

This is a MVC controller back end Angular 8 front end implementation of the LiteracyPro Coding Challenge.

### What is this repository for? ###

* This is Matthew Anderson's submission for the LiteracyProCoding Challenge
* Version 0.1

### How do I get set up? ###

* This needs Visual Studio 2019 and .net core 3.1
* It was intended to run on a SQL Server 2017 database server
* The Angular Client app needs node.js and npm
* Database configuration - The database script is going to assume you are putting the database and log files in the C:\Program Files\Microsoft SQL Server\MSSQL14.SQL2017\MSSQL\DATA\ folder
* The connection string in the appsettings.development.json file is going to assume my database server ".\SQL2017" and will want to use a trusted connection - which means your AD user must have sa rights.
* You will need to run the database script in the root folder called DatabaseScript.sql. This will create the database, 2 tables, and 5 database records. Of course everything ran brilliantly on my machine but if you have any questions please email me.


### Notes: ###

When I moved the code to a different machine it decided to run on port 44365. If this happens to you. Please update the file purchase.service.ts in the clientApp/src folder and update the BASE_URL constant to the proper port number.